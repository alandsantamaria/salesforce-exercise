package com.adsantamaria.commands;

import com.adsantamaria.Directory;
import com.adsantamaria.MockContext;
import com.adsantamaria.parameters.RecursiveParameter;

import java.util.Locale;

public class ListContentsCommand extends CommandWithParameter{

    private static final String COMMAND_NAME="ls";

    public ListContentsCommand () {
        this.parameter = new RecursiveParameter();
    }

    @Override
    public void exec(MockContext mockContext) {
        if (this.parameter.getValue() == null) {
            mockContext.getCurrentDir().getSubDirs().forEach(dir -> System.out.println(dir.getPath()));
            mockContext.getCurrentDir().getFiles().forEach(file -> System.out.println(file.getName()));
        } else {
           listDir(mockContext.getCurrentDir());
        }
    }
    @Override
    public String getName() { return COMMAND_NAME; }

    private void listDir(Directory directory) {
        System.out.println(directory.getPath());
        directory.getFiles().forEach(file -> System.out.println(file.getName()));
        directory.getSubDirs().forEach(dir -> listDir(dir));


    }

}
