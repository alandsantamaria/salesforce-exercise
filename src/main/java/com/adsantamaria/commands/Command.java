package com.adsantamaria.commands;

import com.adsantamaria.MockContext;

public abstract class Command {
    public abstract void exec(MockContext mockContext);
    public abstract String getName();
}
