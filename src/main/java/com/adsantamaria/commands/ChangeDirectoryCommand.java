package com.adsantamaria.commands;

import com.adsantamaria.Directory;
import com.adsantamaria.MockContext;
import com.adsantamaria.parameters.DirNameParameter;

public class ChangeDirectoryCommand extends CommandWithParameter {

    private static final String COMMAND_NAME="cd";
    private static final String RETURN_CODE = "..";

    public ChangeDirectoryCommand () {
        this.parameter = new DirNameParameter();
    }

    @Override
    public void exec(MockContext mockContext) {

        boolean execFinished = false;

        if (RETURN_CODE.equals(this.parameter.getValue()) && mockContext.getCurrentDir().getParentDir() != null) {
            mockContext.setCurrentDir(mockContext.getCurrentDir().getParentDir());
            execFinished = true;
        }else if (RETURN_CODE.equals(this.parameter.getValue()) && mockContext.getCurrentDir().getParentDir() == null) {
            execFinished = true;
        }

        if (!execFinished) {

            boolean dirFound = false;

            for (Directory eachSubDir : mockContext.getCurrentDir().getSubDirs()) {
                if (eachSubDir.getName().equals(this.parameter.getValue())) {
                    mockContext.setCurrentDir(eachSubDir);
                    dirFound = true;
                }
            }

            if (!dirFound)
                System.out.println("Directory not found");
        }
    }

    @Override
    public String getName() { return COMMAND_NAME; }

}
