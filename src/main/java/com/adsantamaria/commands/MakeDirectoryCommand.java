package com.adsantamaria.commands;

import com.adsantamaria.Directory;
import com.adsantamaria.MockContext;
import com.adsantamaria.parameters.DirNameParameter;

public class MakeDirectoryCommand extends CommandWithParameter {

    private static final String COMMAND_NAME="mkdir";

    public MakeDirectoryCommand () {
        this.parameter = new DirNameParameter();
    }

    @Override
    public void exec(MockContext mockContext) {
        boolean dirExists = mockContext.getCurrentDir().getSubDirs().stream().anyMatch(dir -> dir.getName().equals(this.parameter.getValue()));
        if (dirExists) {
            System.out.println("Directory already exists");
        }else{
            Directory newDir = new Directory(this.parameter.getValue());
            newDir.setParentDir(mockContext.getCurrentDir());
            mockContext.getCurrentDir().addDir(newDir);
        }
    }

    @Override
    public String getName() { return COMMAND_NAME; }

}
