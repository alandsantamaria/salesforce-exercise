package com.adsantamaria.commands;

import com.adsantamaria.File;
import com.adsantamaria.MockContext;
import com.adsantamaria.parameters.FileNameParameter;

public class CreateFileCommand extends CommandWithParameter {

    private static final String COMMAND_NAME="touch";

    public CreateFileCommand () {
        this.parameter = new FileNameParameter();
    }

    @Override
    public void exec(MockContext mockContext) {
        File file = new File(this.parameter.getValue());
        mockContext.getCurrentDir().addFile(file);
    }

    @Override
    public String getName() { return COMMAND_NAME; }

}
