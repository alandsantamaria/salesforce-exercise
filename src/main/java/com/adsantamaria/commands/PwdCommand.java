package com.adsantamaria.commands;

import com.adsantamaria.MockContext;

public class PwdCommand extends Command{

    private static final String COMMAND_NAME="pwd";

    @Override
    public void exec(MockContext mockContext) {
        System.out.println(mockContext.getCurrentDir().getPath());
    }

    @Override
    public String getName() { return COMMAND_NAME; }

}
