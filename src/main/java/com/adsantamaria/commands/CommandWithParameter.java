package com.adsantamaria.commands;

import com.adsantamaria.parameters.Parameter;

public abstract class CommandWithParameter extends Command{

    protected Parameter parameter;

    public Parameter getParameter() {
        return this.parameter;
    }

}
