package com.adsantamaria.commands;

import com.adsantamaria.MockContext;

public class QuitCommand extends Command{

    private static final String COMMAND_NAME="quit";

    @Override
    public void exec(MockContext mockContext) {
        System.exit(0);
    }

    @Override
    public String getName() { return COMMAND_NAME; }

}
