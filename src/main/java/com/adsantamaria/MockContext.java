package com.adsantamaria;

public class MockContext {

    public void generateMockContext() {
        Directory root = new Directory("root");
        File fileA = new File("a.txt");
        File fileB = new File("b.txt");
        File fileC = new File("c.txt");
        root.addFile(fileA);
        root.addFile(fileB);
        root.addFile(fileC);

        Directory dirA = new Directory("dirA");
        dirA.addFile(fileA);
        dirA.addFile(fileB);
        dirA.addFile(fileC);
        dirA.setParentDir(root);

        Directory dirB = new Directory("dirB");
        dirB.addFile(fileA);
        dirB.addFile(fileB);
        dirB.addFile(fileC);
        dirB.setParentDir(root);

        Directory dirC = new Directory("dirC");
        dirC.addFile(fileA);
        dirC.addFile(fileB);
        dirC.addFile(fileC);
        dirC.setParentDir(root);

        root.addDir(dirA);
        root.addDir(dirB);
        root.addDir(dirC);

        this.setCurrentDir(dirC);

    }

    private Directory currentDir;

    public void setCurrentDir(Directory currentDir) {
        this.currentDir = currentDir;
    }

    public Directory getCurrentDir() {
        return this.currentDir;
    }

}
