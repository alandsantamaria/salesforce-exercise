package com.adsantamaria;

import com.adsantamaria.exceptions.InvalidCommandException;
import com.adsantamaria.exceptions.UnrecognizedCommandException;
import com.adsantamaria.commands.Command;

import java.util.Scanner;

public class Solution {

    public static void main(String args[] ) throws Exception {
        Scanner in = new Scanner(System.in);
        CommandParser commandParser = new CommandParser();
        MockContext mockContext = new MockContext();
        mockContext.generateMockContext();
        while (true) {
            try {
                Command command = commandParser.getCommand(in.nextLine());
                command.exec(mockContext);
            } catch (UnrecognizedCommandException | InvalidCommandException e) {
                System.out.println(e.getMessage());
            }
        }
    }

}
