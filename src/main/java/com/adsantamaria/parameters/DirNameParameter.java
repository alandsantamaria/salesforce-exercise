package com.adsantamaria.parameters;

import com.adsantamaria.exceptions.InvalidCommandException;

public class DirNameParameter extends Parameter{

    @Override
    public void setValue(String value) throws InvalidCommandException {
        if (value != null) {
            this.value = value;
        }else{
            throw new InvalidCommandException();
        }
    }
}
