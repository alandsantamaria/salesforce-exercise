package com.adsantamaria.parameters;

import com.adsantamaria.exceptions.InvalidCommandException;

public class RecursiveParameter extends Parameter {
    @Override
    public void setValue(String value) throws InvalidCommandException {
        if (value != null && !"-r".equals(value)) {
            throw new InvalidCommandException();
        } else if (value != null) {
            this.value = value;
        } else{
            this.value = null;
        }
    }
}
