package com.adsantamaria.parameters;

import com.adsantamaria.exceptions.InvalidCommandException;

public abstract class Parameter {
    protected String value;

    public String getValue() {
        return this.value;
    }
    public abstract void setValue(String value) throws InvalidCommandException;

}
