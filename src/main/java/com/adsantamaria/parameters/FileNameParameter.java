package com.adsantamaria.parameters;

import com.adsantamaria.exceptions.InvalidCommandException;

public class FileNameParameter extends Parameter{

    private static final int CHARACTER_LIMIT = 100;

    @Override
    public void setValue(String value) throws InvalidCommandException {
        if (value != null && value.length() <= CHARACTER_LIMIT) {
            this.value = value;
        }else{
            throw new InvalidCommandException();
        }
    }
}
