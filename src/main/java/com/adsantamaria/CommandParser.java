package com.adsantamaria;

import com.adsantamaria.commands.*;
import com.adsantamaria.exceptions.InvalidCommandException;
import com.adsantamaria.exceptions.UnrecognizedCommandException;

import java.util.ArrayList;
import java.util.List;

public class CommandParser {

    private List<Command> availableCommands;

    public CommandParser() {
        availableCommands = new ArrayList<>();
        availableCommands.add(new PwdCommand());
        availableCommands.add(new QuitCommand());
        availableCommands.add(new CreateFileCommand());
        availableCommands.add(new MakeDirectoryCommand());
        availableCommands.add(new ListContentsCommand());
        availableCommands.add(new ChangeDirectoryCommand());
    }
    public Command getCommand(String command) throws UnrecognizedCommandException, InvalidCommandException {
        String[] spplitedLine = command.split("\\s+");
        String commandCandidate = spplitedLine[0];
        String argumentCandidate = null;
        if (spplitedLine.length > 1)
            argumentCandidate = spplitedLine[1];
        Command foundCommand = null;
        for (Command eachAvailableCommand : availableCommands) {
            if (commandCandidate.equals(eachAvailableCommand.getName())) {
                foundCommand = eachAvailableCommand;
                break;
            }
        }

        if (foundCommand != null) {
            if (foundCommand.getClass().getSuperclass() == CommandWithParameter.class) {
                ((CommandWithParameter)foundCommand).getParameter().setValue(argumentCandidate);
            }
            return foundCommand;
        } else {
            throw new UnrecognizedCommandException();
        }
    }

}
