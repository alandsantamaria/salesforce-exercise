package com.adsantamaria;

import java.util.ArrayList;
import java.util.List;

public class Directory {

    private String name;
    private Directory parentDir;
    private List<File> files;
    private List<Directory> subDirs;

    private static final String SEPARATOR = "/";

    public Directory(String name) {
        this.name = name;
        this.files = new ArrayList<>();
        this.subDirs = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setParentDir(Directory parentDir) {
        this.parentDir = parentDir;
    }

    public Directory getParentDir() {
        return this.parentDir;
    }

    public void addFile(File newFile) {
        this.files.add(newFile);
    }

    public List<File> getFiles() {
        return files;
    }

    public void addDir(Directory dir) {
        this.subDirs.add(dir);
    }

    public String getPath() {
        StringBuilder path = new StringBuilder( SEPARATOR);
        path.insert(0,SEPARATOR.concat(this.getName()));
        Directory parentDirectory = this.parentDir;
        while(parentDirectory != null) {
            path.insert(0,SEPARATOR.concat(parentDirectory.getName()));
            parentDirectory = parentDirectory.getParentDir();
        }
        return path.toString();
    }

    public List<Directory> getSubDirs() {
        return this.subDirs;
    }
}
