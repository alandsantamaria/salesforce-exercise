package com.adsantamaria.exceptions;

public class UnrecognizedCommandException extends Exception{

    public UnrecognizedCommandException() {
        super("Unrecognized command");
    }
}
